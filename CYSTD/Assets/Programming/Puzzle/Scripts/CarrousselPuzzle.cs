using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CarrousselPuzzle : Puzzle
{
    [SerializeField]
    GameObject _base;
    [SerializeField]
    List<float> _anglesToReach;
    [SerializeField]
    float _angleCheckOffset;
    [SerializeField]
    List<GameObject> _progressHints;
    [SerializeField]
    Material _progressHintsTexture;
    [SerializeField] [Range(0.1f, 1f)] float _turnSpeed;
    [SerializeField] [Range(0.0f, 10f)] float _jammingMinSeconds;
    [SerializeField] [Range(0.0f, 10f)] float _jammingMaxSeconds;
    [SerializeField] GameObject _jammingWarning;
    [SerializeField] bool _isJammedOnStart;

    InputAction _leftRight;
    float _movementIndicator = 0;
    int _currentIndex = 0;
    bool _isJammed;
    float _secondsToJam;

    protected override void Start()
    {
        base.Start();
        _leftRight = _puzzleActions.FindAction("LeftRight");
        _puzzleActions.FindAction("Action").performed += _FixJamming;
        _isJammed = _isJammedOnStart;
        if (!_isJammed) _PrepareNextJamming();
        _jammingWarning.SetActive(false);
    }

    void Update()
    {
        if (IsActive())
        {
            _GetInput();
            if (!_isJammed) _Rotate();
            if (_CheckAngle()) _NextPoint(); 
        }
    }

    bool _CheckAngle()
    {
        if (_anglesToReach.Count > _currentIndex) return _base.transform.localEulerAngles.y >= _anglesToReach[_currentIndex] - _angleCheckOffset &&
                                                         _base.transform.localEulerAngles.y <= _anglesToReach[_currentIndex] + _angleCheckOffset;
        return false;
    }

    void _GetInput()
    {
        _movementIndicator = _leftRight.ReadValue<float>();
    }

    void _Rotate()
    {
        if (_movementIndicator != 0) _secondsToJam -= Time.deltaTime;
        if (_secondsToJam < Mathf.Epsilon)
        {
            _isJammed = true;
            StartCoroutine(_JammingWarning());
            return;
        }
        _base.transform.Rotate(Vector3.up, _movementIndicator * _turnSpeed * -1);
    }

    void _NextPoint()
    {
        if (_progressHints.Count > _currentIndex && _progressHints[_currentIndex].TryGetComponent(out MeshRenderer mr)) mr.material = _progressHintsTexture;
        _currentIndex++;
        if (_progressHints.Count <= _currentIndex) Complete();
    }

    void _FixJamming(InputAction.CallbackContext ctx)
    {
        if (_isJammed) _PrepareNextJamming();
    }

    void _PrepareNextJamming()
    {
        _secondsToJam = Random.Range(_jammingMinSeconds, _jammingMaxSeconds);
        _isJammed = _secondsToJam < Mathf.Epsilon;
    }

    IEnumerator _JammingWarning()
    {
        while (_isJammed)
        {
            _jammingWarning.SetActive(!_jammingWarning.activeInHierarchy);
            yield return new WaitForSecondsRealtime(0.2f);
        }
        _jammingWarning.SetActive(false);
    }
}
