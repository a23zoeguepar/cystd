using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabbable : MonoBehaviour
{
    bool _grabbed;
    public bool Grabbed
    {
        get => _grabbed;
        set
        {
            _grabbed = value;
            this.gameObject.SetActive(!_grabbed);
        }
    }

    [SerializeField]
    ItemType _itemType;
    public ItemType ItemType
    {
        get => _itemType;
        set { _itemType = value; }
    }

    protected void Start()
    {
        GrabList.Instance.AddToList(this);
    }
}
