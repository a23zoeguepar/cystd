using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSystem : MonoBehaviour
{
    public List<Light> _light1; //cambiar a private
    public List<Light> _light2; //cambiar a private

    float lightForce;

    private void Start()
    {
        List<GameObject> lightGameObject = new List<GameObject>();
        foreach (Transform child in transform)
        {
            lightGameObject.Add(child.gameObject);
        }
        foreach (GameObject childLight in lightGameObject)
        {
            Light[] light = childLight.GetComponentsInChildren<Light>();

            if (light != null)
            {
                foreach (Light l in light)
                {
                    if (l.gameObject.tag == "Light")
                    {
                        _light1.Add(l);
                    }
                    if (l.gameObject.tag == "Light2")
                    {
                        _light2.Add(l);
                    }
                }

            }
        }

        lightForce = _light1[0].intensity; // lightForce = fuerza inicial
        lightForce = _light2[0].intensity; // lightForce = fuerza inicial

        generateColliders(_light1, _light1[0].intensity);
        generateColliders(_light2, _light2[0].intensity);
    }

    private void Update()
    {
        //t += 0.5f * Time.deltaTime;
        //lightForce = Mathf.Lerp(lightForce, 0, t);
        foreach (Light light in _light1)
        {
            //light.intensity = lightForce;
            light.intensity = Timer.Instance.LightPercentage;
        }
        foreach (Light light in _light2)
        {
            //light.intensity = lightForce;
            light.intensity = Timer.Instance.LightPercentage;
        }
    }

    private void generateColliders(List<Light> lights, float value)
    {
        foreach (Light light in lights)
        {
            SphereCollider cC = light.gameObject.AddComponent<SphereCollider>();
            cC.radius = light.range * 0.5f;
            cC.isTrigger = true;
            light.intensity = value;
        }
    }


}
