using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionChain : BaseMission
{
    [SerializeField]
    int missionsBackOnFail;
    [SerializeField]
    List<BaseMission> _orderedSubmissions;
    int _currentPos;

    public List<BaseMission> OrderedSubmissions { get => _orderedSubmissions; set => _orderedSubmissions = value; }

    void Start()
    {
        OrderedSubmissions ??= new List<BaseMission>();
        if (OrderedSubmissions.Count > 1) OrderedSubmissions.GetRange(1, OrderedSubmissions.Count - 1).ForEach(m => m.Disable());
        _currentPos = 0;
    }

    void Update()
    {
        if (!UpdateValidations()) return;
        BaseMission currentMission = OrderedSubmissions[_currentPos];
        if (currentMission.IsFailed()) PreviousMission();
        else if (currentMission.IsCompleted()) NextMission();
    }

    public override string GetMissionExplaination()
    {
        if (_currentPos >= OrderedSubmissions.Count) return null;
        return OrderedSubmissions[_currentPos].GetMissionExplaination();
    }

    void PreviousMission()
    {
        OrderedSubmissions[_currentPos].ResetMission();
        OrderedSubmissions[_currentPos].Disable();
        int prevMissionPos = _currentPos - missionsBackOnFail;
        if (prevMissionPos < 0) prevMissionPos = 0;
        _currentPos = prevMissionPos;
        OrderedSubmissions[_currentPos].Enable();
        OrderedSubmissions[_currentPos].ResetMission();
    }

    void NextMission() 
    {
        OrderedSubmissions[_currentPos].Disable();
        _currentPos++;
        if (_currentPos == OrderedSubmissions.Count)
        {
            _CompleteMission();
            return;
        }
        OrderedSubmissions[_currentPos].Enable();
    }

    public override void Enable()
    {
        base.Enable();
        if (_currentPos < OrderedSubmissions.Count) OrderedSubmissions[_currentPos].Enable();
    }

    public override void Disable()
    {
        base.Disable();
        if (_currentPos < OrderedSubmissions.Count) OrderedSubmissions[_currentPos].Disable();
    }

    public override bool IsCompleted()
    {
        return OrderedSubmissions.TrueForAll(m => m.IsCompleted());
    }
}
