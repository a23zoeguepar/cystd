using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabList : MonoBehaviour
{
    public List<Grabbable> GrabbableList = new List<Grabbable>();
    public static GrabList Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void AddToList(Grabbable grabItem)
    {
        GrabbableList.Add(grabItem);
    }
}
