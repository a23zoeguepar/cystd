
public enum MissionName
{
    RecreationalArea,
    GetCableGameMachine,
    ConnectCableGameMachine,
    GameMachine,
    GetKey,
    KeyToFerrisWheel,
    Carrousel,
    CarrouselPuzzle,
    GetCableCarrousel,
    CableToFerrisWheel,
    JumpingSeal,
    Seal,
    GetLever,
    LeverToFerrisWheel,
    Battery,
    GetBattery,
    BatteryToFerrisWheel,
    FerrisWheel,
    TurnOnFerrisWheel,
    Exit,
    Notebook,
    Generator
}
